//
//  ViewController.swift
//  act6
//
//  Created by prueba on 2/26/19.
//  Copyright © 2019 prueba. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var currentTemp: UILabel!
    @IBOutlet weak var minTemo: UILabel!
    @IBOutlet weak var maxTemp: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var air: UILabel!
    
    
    let city = ["London","New York","Monterrey","Spain","Finland"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return city.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return city[row]
    }
    
    @IBOutlet weak var scroller: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scroller.delegate = self
        scroller.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
        let personUrl = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=Monterrey&appid=fecbe7fbcf8c58ecc3c3756d7d59ae61")
        URLSession.shared.dataTask(with: personUrl!) { (data, response, error) in
            guard let data = data else { return }
            do {
                let json = try JSON(data: data)
                if let main = json["weather"][0]["main"].string{
                    print("Weather: ", main)
                    DispatchQueue.main.async {
                        self.weather.text = "Weather: \(main)";
                    }
                }
                if let currentTempK = json["main"]["temp"].float{
                    let currentTempC = currentTempK - 273.15
                    print("Current Tempertature: ", currentTempC)
                    DispatchQueue.main.async {
                        self.currentTemp.text = "Current Tempertature: \(currentTempC)";
                    }
                }
                if let minTempK = json["main"]["temp_min"].float{
                    let minTempC = minTempK - 273
                    print("Minimum Tempertature: ", minTempC)
                    DispatchQueue.main.async {
                        self.minTemo.text = "Minimum Temperature: \(minTempC)";
                    }
                }
                if let maxTempK = json["main"]["temp_max"].float{
                    let maxTempC = maxTempK - 273
                    print("Maximum Tempertature: ", maxTempC)
                    DispatchQueue.main.async {
                        self.maxTemp.text = "Maximum Tempertature: \(maxTempC)";
                    }
                }
                if let humidityS = json["main"]["humidity"].float{
                    print("Humidity: ", humidityS)
                    DispatchQueue.main.async {
                        self.humidity.text = "Humidity: \(humidityS)";
                    }
                }
                if let speedAir = json["wind"]["speed"].float{
                    print("Speed Air: ", speedAir)
                    DispatchQueue.main.async {
                        self.air.text = "Air Speed: \(speedAir)";
                    }
                }
            } catch let err {
                print("Err", err)
            }
            }.resume()
    }
}

